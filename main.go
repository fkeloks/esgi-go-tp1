package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

type authorEntryStruct struct {
	author string
	entry  string
}

func timeHandler(res http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		fmt.Fprintln(res, "Bad method")

		return
	}

	fmt.Fprintf(res, time.Now().Format("15h04"))
}

func addHandler(res http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		fmt.Fprintln(res, "Bad method")

		return
	}

	if err := req.ParseForm(); err != nil {
		fmt.Fprintln(res, "Bad request")

		return
	}

	if !req.Form.Has("author") || req.Form.Get("author") == "" || !req.Form.Has("entry") || req.Form.Get("entry") == "" {
		fmt.Fprintln(res, "Bad parameters")

		return
	}

	authorEntry := authorEntryStruct{
		author: req.Form.Get("author"),
		entry:  req.Form.Get("entry"),
	}

	f, err := os.OpenFile("entries.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	if _, err := f.Write([]byte(authorEntry.author + ":" + authorEntry.entry + "\n")); err != nil {
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(res, authorEntry.author+":"+authorEntry.entry)
}

func entriesHandler(res http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		fmt.Fprintln(res, "Bad method")

		return
	}

	filer, err := os.Open("entries.txt")
	if err != nil {
		log.Fatal(err)
	}

	defer filer.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(filer)

	fmt.Fprintf(res, "%v", buf.String())
}

func main() {
	http.HandleFunc("/", timeHandler)
	http.HandleFunc("/add", addHandler)
	http.HandleFunc("/entries", entriesHandler)

	http.ListenAndServe(":4567", nil)
}
